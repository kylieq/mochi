const express = require('express');
const app = express();
const axios = require('axios');
const port = 3000;

app.set('view engine', 'ejs');

require('dotenv').config();

// Body-parser allows us to grab information from the POST
const bodyParser = require('body-parser');
app.use(bodyParser.json()); // support json encoded bodies
const urlencodedParser = bodyParser.urlencoded({ extended: false });

app.use(express.static('public'));
app.use(express.static('routes'));
app.use(express.static('controllers'));

const ingredientsRouter = require('./routes/ingredients'); 
app.use('/ingredients', ingredientsRouter);

app.get('/', function(req, res) {
    res.render('index');
});

app.post('/recipes', urlencodedParser, function(req, res) {
    let txt = '';
    let ingredients = req.body.food;
    if (ingredients == null) { res.render('ingredients'); }

    else {
        let recipeTitles = new Map();

        var i, j;
        for (i=0; i<ingredients.length; i++) {
            convertToString(ingredients[i]);
        }

        function convertToString(value) {
            txt = txt + value.toLowerCase() + '%2C'
        }

        // API that takes list of ingredients and returns recipes.
        const ingredientApiCall = () => {
            return new Promise((resolve, reject) => {
                let response = axios({
                    "method":"GET",
                    "url":"https://spoonacular-recipe-food-nutrition-v1.p.rapidapi.com/recipes/findByIngredients",
                    "headers":{
                        "content-type":"application/json",
                        "x-rapidapi-host":"spoonacular-recipe-food-nutrition-v1.p.rapidapi.com",
                        "x-rapidapi-key":process.env.API_PWD,
                        "useQueryString":true
                    },
                    "params":{
                        "number":"50",
                        "ranking":"1",
                        "ignorePantry":"false",
                        "ingredients":txt
                    }
                });
                resolve(response);
            });
        }

        ingredientApiCall().then((response) => {
            //console.log(response);
            for (i=0; i<response.data.length; i++) {
                recipeTitles.set(response.data[i].id, response.data[i].title);
            }
            res.render('recipes', { recipe_list: recipeTitles });
        })
        .catch((err) => { console.log(err) });
    }
});

app.post('/recipeInformation', urlencodedParser, function(req, res) {
    let recipeID = req.body.recipe;

    let recipeInstructions = [];
    let recipeIngredients = [];
    let recipeSummaries = '';

    // API that takes recipe ID and returns information.
    const infoApiCall = (recipeID) => {
        let url = "https://spoonacular-recipe-food-nutrition-v1.p.rapidapi.com/recipes/"+recipeID+"/information";
        return new Promise((resolve, reject) => {
            let response = axios({
                "method":"GET",
                "url":url,
                "headers":{
                    "content-type":"application/json",
                    "x-rapidapi-host":"spoonacular-recipe-food-nutrition-v1.p.rapidapi.com",
                    "x-rapidapi-key":process.env.API_PWD,
                    "useQueryString":true
                },
                "params":{
                    "id":recipeID
                }
            });
            resolve(response);
        });
    }

    infoApiCall(recipeID).then((response) => {
        // Get unique recipe ID
        recipeId = response.data.id;

        // Get recipe instructions
        let instructionsLength = response.data.analyzedInstructions.length;
        if (instructionsLength > 0) {
            recipeInstructions = response.data.analyzedInstructions[0].steps;
        }
        else {
            recipeInstructions = [];
        }

        // Get recipe summary
        recipeSummaries = response.data.summary;

        // Get ingredients + quantities
        ingredients = response.data.extendedIngredients;
        for (j=0; j<ingredients.length; j++) {
            recipeIngredients.push(ingredients[j].original);
        }

        res.render('recipeInformation', { title:response.data.title, summary:recipeSummaries, ingredients:recipeIngredients, steps:recipeInstructions });
    })
    .catch((err) => { console.log(err) });
});

app.listen(port, () => {
  console.log(`Mochi app listening at http://localhost:${port}`);
});

module.exports = app;