// Display ingredients page.
exports.index = function(req, res, next) {
	res.render('ingredients');
};

// Display list of selected ingredients.
exports.recipes_list = function(req, res, next) {
	res.redirect('recipes');
};