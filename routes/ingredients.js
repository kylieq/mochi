var express = require('express');
var router = express.Router();

// Require controller modules.
var ingredients_controller = require('../controllers/ingredientsController');

/// INGREDIENTS ROUTES ///

// GET ingredients page.
router.get('/', ingredients_controller.index); // This should map to /ingredients/ because we import the route with a /ingredients prefix

// GET request for list of all selected ingredients.
router.get('/recipes', ingredients_controller.recipes_list);

module.exports = router;