const axios = require('axios');
const BASE_URL = 'https://api.spoonacular.com/recipes/findByIngredients';

module.exports = {
	getRecipes: (ingredients) => axios({
		method: 'GET',
		url: BASE_URL,
		headers: {
			'content-type': 'application/json'
		},
		params: {
			ingredients: ingredients,
			number: 10,
			limitLicense: true,
			ranking: 1,
			ignorePantry: true
		}
	})
}