// Toggle visibility of ingredient categories.
function vis_toggle(x) {
	// Toggle visibility for category 'x'.
	if (x.style.display === "none") {
		x.style.display = "block";
	}			
	else {
		x.style.display = "none";
	}
}

// Get all selected ingredients and display in alert.
function printChecked() {
	let allCheckBoxes = document.getElementsByTagName('input');
	var selectedBoxes = [];
	for(var i=0; i<allCheckBoxes.length; i++) {
		if(allCheckBoxes[i].type=='checkbox' && allCheckBoxes[i].checked==true)
			selectedBoxes.push(allCheckBoxes[i].value);
	}
	console.log(selectedBoxes);
	return selectedBoxes;
}

// Event handler for when 'Submit Ingredients' is clicked.
/*$("#submitbtn").submit(function(event) {
	let arr = printChecked();
	console.log('Returned array: ' + arr);
	window.localStorage.setItem('ingredients', arr);

	event.preventDefault();
});
*/
//$("#submitbtn").submit(function(event) {
	//event.preventDefault();
	//let arr = printChecked();
	//$.post('/selectedingredients.html', { arr },
	//function(data, status) {
	//	alert('Data: ' + data);
	//});
//});